#include "list.h"
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <cmocka.h>

static void test_dll_remove(void **state) {
  (void)state; // unused

  dll_t *list = dll_create();
  assert_non_null(list);

  dll_insert(list, 50, 0);
  dll_insert(list, 60, 1);

  int result_remove = dll_remove(list, 0);
  assert_int_equal(result_remove, 0);

  int value;
  int result_get_head = dll_get(list, &value, 0);
  assert_int_equal(result_get_head, 0);
  assert_int_equal(value, 60); // The value 60 should now be the first element
}

static void test_dll_remove_out_of_bounds(void **state) {
  (void)state; // unused

  dll_t *list = dll_create();
  assert_non_null(list);

  dll_insert(list, 70, 0);
  int result = dll_remove(list, 1); // Out of bounds
  assert_true(result < 0);
}

int main(void) {
  const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_dll_remove),
      cmocka_unit_test(test_dll_remove_out_of_bounds),
  };
  cmocka_set_message_output(CM_OUTPUT_XML);
  return cmocka_run_group_tests_name("list-remove", tests, NULL, NULL);
}
