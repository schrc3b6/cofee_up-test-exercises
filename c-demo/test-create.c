#include "list.h"
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <cmocka.h>

static void test_dll_create(void **state) {
  (void)state; // unused

  dll_t *list = dll_create();
  assert_non_null(list);
}

int main(void) {
  const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_dll_create),
  };
  cmocka_set_message_output(CM_OUTPUT_XML);
  return cmocka_run_group_tests_name("list-create", tests, NULL, NULL);
}
