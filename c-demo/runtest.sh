#!/bin/bash
#
# build tests
make list.o

unset IS_DLL_CREATE_IMPLEMENTED
unset IS_DLL_INSERT_IMPLEMENTED
unset IS_DLL_GET_IMPLEMENTED
unset IS_DLL_REMOVE_IMPLEMENTED

nm list.o | grep "T dll_create" &> /dev/null
if [ $? -eq 0 ]; then
  IS_DLL_CREATE_IMPLEMENTED=true
else
  echo "Fehler: Die Methode dll_create wurde nicht in deinem Source Code gefunden! Bitte implementiere diese um sie zu testen." > results/create.makecm
fi

nm list.o | grep "T dll_insert" &> /dev/null
if [ $? -eq 0 ]; then
  IS_DLL_INSERT_IMPLEMENTED=true
else
  echo "Fehler: Die Methode dll_insert wurde nicht in deinem Source Code gefunden! Bitte implementiere diese um sie zu testen." > results/insert.makecm
fi

nm list.o | grep "T dll_get" &> /dev/null
if [ $? -eq 0 ]; then
  IS_DLL_GET_IMPLEMENTED=true
else
  echo "Fehler: Die Methode dll_get wurde nicht in deinem Source Code gefunden! Bitte implementiere diese um sie zu testen." > results/get.makecm
fi

nm list.o | grep "T dll_remove" &> /dev/null
if [ $? -eq 0 ]; then
  IS_DLL_REMOVE_IMPLEMENTED=true
else
  echo "Fehler: Die Methode dll_remove wurde nicht in deinem Source Code gefunden! Bitte implementiere diese um sie zu testen." > results/remove.makecm
fi


if [[ -v IS_DLL_CREATE_IMPLEMENTED ]]; then
  make test-create.out

  if [[ -v IS_DLL_INSERT_IMPLEMENTED ]]; then
    make test-insert.out

    if [[ -v IS_DLL_GET_IMPLEMENTED ]]; then
      make test-get.out

      if [[ -v IS_DLL_REMOVE_IMPLEMENTED ]]; then
        make test-remove.out
      fi
    fi
  fi
fi

unset IS_STACK_CREATE_IMPLEMENTED
unset IS_STACK_LENGTH_IMPLEMENTED
unset IS_STACK_PUSH_IMPLEMENTED
unset IS_STACK_POP_IMPLEMENTED



if compgen -G "./test*.out" > /dev/null; then
for i in test*.out; do
  echo "Teste $i";
        if "./$i" > /dev/null 2> "results/$i.make"; then
          rm -f "results/$i.make";
        else
          if [ $? -ge 125 ] && [ $? -lt 255 ]; then
            echo "Fehler beim ausfuehren der Unit-Tests, möglicher Segmentation Fault oder unerwartetes Signal" > results/output.makecm;
          else
            rm -f "results/$i.make";
          fi;
          ERROR=1;
        fi;
      done
fi
if [[ -v ERROR ]]; then false; fi
