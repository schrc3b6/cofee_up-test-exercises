#include "list.h"
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <cmocka.h>

static void test_dll_insert(void **state) {
  (void)state; // unused

  dll_t *list = dll_create();
  assert_non_null(list);

  int result = dll_insert(list, 1, 0);
  assert_int_equal(result, 0);
}

static void test_dll_insert_out_of_bounds(void **state) {
  (void)state; // unused

  dll_t *list = dll_create();
  assert_non_null(list);

  int result = dll_insert(list, 20, 1); // Out of bounds
  assert_true(result < 0);
}

int main(void) {
  const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_dll_insert),
      cmocka_unit_test(test_dll_insert_out_of_bounds),
  };
  cmocka_set_message_output(CM_OUTPUT_XML);
  return cmocka_run_group_tests_name("list-insert", tests, NULL, NULL);
}
