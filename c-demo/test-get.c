#include "list.h"
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <cmocka.h>

static void test_dll_insert_and_get(void **state) {
  (void)state; // unused

  dll_t *list = dll_create();
  assert_non_null(list);

  int result_insert = dll_insert(list, 10, 0);
  assert_int_equal(result_insert, 0);

  int value;
  int result_get = dll_get(list, &value, 0);
  assert_int_equal(result_get, 0);
  assert_int_equal(value, 10);
}

static void test_dll_get_out_of_bounds(void **state) {
  (void)state; // unused

  dll_t *list = dll_create();
  assert_non_null(list);

  dll_insert(list, 40, 0);
  int value;
  int result = dll_get(list, &value, 1); // Out of bounds
  assert_true(result < 0);
}

int main(void) {
  const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_dll_insert_and_get),
      cmocka_unit_test(test_dll_get_out_of_bounds),
  };
  cmocka_set_message_output(CM_OUTPUT_XML);
  return cmocka_run_group_tests_name("list-get", tests, NULL, NULL);
}
